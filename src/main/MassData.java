package main;

public class MassData {
	private float mass;
	private float inverseMass;
	
	private float inertia;
	private float inverseInertia;
	
	
	public MassData(float mass, float inertia) {
		setMass(mass);
		setInertia(inertia);
	}
	public float getMass() {
		return mass;
	}
	public void setMass(float mass) {
		this.mass = mass;
		setInverseMass();
	}
	public float getInverseMass() {
		return inverseMass;
	}
	private void setInverseMass() {
		this.inverseMass = 1.0f / mass;
	}
	public float getInertia() {
		return inertia;
	}
	public void setInertia(float inertia) {
		this.inertia = inertia;
		setInverseInertia();
	}
	public float getInverseInertia() {
		return inverseInertia;
	}
	private void setInverseInertia() {
		this.inverseInertia = 1.0f / inertia;
	}
}
