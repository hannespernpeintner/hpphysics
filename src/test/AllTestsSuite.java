package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({BodyTest.class, EntityTest.class, MathTest.class})
public class AllTestsSuite {

}
