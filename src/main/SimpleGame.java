package main;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;


public class SimpleGame extends org.newdawn.slick.BasicGame {
	
	private World world;

	public SimpleGame(String title) {
		super(title);
		world = new World();
	}

	@Override
	public void render(GameContainer arg0, Graphics arg1) throws SlickException {
		world.render(arg1);
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		arg0.setTargetFrameRate(world.fps);
		arg0.setShowFPS(true);
	}

	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
		world.update(world.dt);
	}
	
	public void mouseClicked(int button, int x, int y, int clickCount) {
		if (button == 0) {
			world.addEntity(new Entity(x, y));
		} else if (button == 1) {
			world.addEntity(new Entity(x, y, Float.POSITIVE_INFINITY));
		}
	}
	
	public static void main(String[] args)
	{
		try
		{
			AppGameContainer appgc;
			appgc = new AppGameContainer(new SimpleGame("Simple World"));
			appgc.setDisplayMode(640, 480, false);
			appgc.start();
		}
		catch (SlickException ex)
		{
			Logger.getLogger(SimpleGame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
