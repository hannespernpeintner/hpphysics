package math;

import org.newdawn.slick.geom.Vector2f;

public class Helper {

	public static float cosDot(Vector2f a, Vector2f b) {
		return (float) Math.cos(dot(a, b));
	}

	public static float acosDot(Vector2f a, Vector2f b) {
		return (float) Math.acos(dot(a, b));
	}

	public static float dot(Vector2f a, Vector2f b) {
		return (float) (a.x * b.x + a.y * b.y);
	}

	public static float cross(Vector2f a, Vector2f b) {
		return (a.x * b.y) - (a.y * b.x);
	}

	public static Vector2f cross(Vector2f a, float s) {
		return new Vector2f(s * a.y, -s * a.x);
	}

	public static Vector2f cross(float s, Vector2f a) {
		return new Vector2f(-s * a.y, s * a.x);
	}

	public static Vector2f reflect(Vector2f in, Vector2f normal) {

		Vector2f i = in.copy();
		return i.sub(mul(mul(normal.normalise(), i), -2.0f));
	}

	public static Vector2f mul(Vector2f a, Vector2f b) {
		Vector2f temp = a.copy();
		temp.x *= b.x;
		temp.y *= b.y;
		return temp;
	}

	public static Vector2f mul(Vector2f in, float f) {
		Vector2f temp = in.copy();
		temp.x *= f;
		temp.y *= f;
		return temp;
	}

	public static Vector2f perp(Vector2f in) {
		Vector2f temp = in.copy();
		float temp2 = temp.x;
		temp.x = -temp.y;
		temp.y = temp2;
		return temp;
	}

	public static Vector2f add(Vector2f a, Vector2f b) {
		return a.copy().add(b);
	}
	public static Vector2f sub(Vector2f a, Vector2f b) {
		return a.copy().sub(b);
	}
	
	public static double pointToLineDistance(Vector2f A, Vector2f B, Vector2f P) {
		double normalLength = Math.sqrt((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y));
		return Math.abs((P.x - A.x) * (B.y - A.y) - (P.y - A.y) * (B.x - A.x)) / normalLength;
	}

	public static int findSide(double ax, double ay, double bx, double by,
			double cx, double cy) {

		double slope = (by - ay) / (bx - ax);
		double yIntercept = ay - ax * slope;
		double cSolution = (slope * cx) + yIntercept;
		if (slope != 0) {
			if (cy > cSolution) {
				return bx > ax ? 1 : -1;
			}
			if (cy < cSolution) {
				return bx > ax ? -1 : 1;
			}
			return 0;
		}
		return 0;
	}
}
