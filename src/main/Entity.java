package main;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;


public class Entity {
	
	private Body body;
	
	public Entity(int x, int y) {
		body = new Body(new Rectangle(x, y, 100, 100));
	}

	public Entity(int x, int y, float mass) {
		body = new Body(new Rectangle(x, y, 100, 100), mass);
	}

	public void render(Graphics g) throws SlickException {
		g.draw(body.getPolygon());
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}
}
