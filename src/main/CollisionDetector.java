package main;

import static math.Helper.add;
import static math.Helper.dot;
import static math.Helper.mul;
import static math.Helper.pointToLineDistance;
import static math.Helper.sub;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.GeomUtil;
import org.newdawn.slick.geom.GeomUtil.HitResult;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

public class CollisionDetector {

	public List<CollisionPair> broadphaseCollisions = new ArrayList<>();
	public List<Manifold> collisions = new ArrayList<>();

	List<CollisionPair> getBroadPhaseCollisions(List<Entity> entities) {
		List<CollisionPair> pairs = new ArrayList<>();
		
		for (Entity a : entities) {
			for (Entity b : entities) {
				
				if (a.equals(b)) break; 
				
				Circle aCircle = a.getBody().getBoundingCircle();
				Circle bCircle = b.getBody().getBoundingCircle();
				
				if(aCircle.intersects(bCircle)) {
					CollisionPair pair = new CollisionPair(a, b);
					addIfDoesntContain(pairs, pair);
				}
			}
		}
		return pairs;
	}
	
	private void addIfDoesntContain(List<CollisionPair> pairs, CollisionPair pair) {
		if (!pairs.contains(pair)) {
			pairs.add(pair);
		}
	}

	public List<Manifold> getCollisions(List<CollisionPair> broadphaseCollisions) {
		List<Manifold> collisions = new ArrayList<>();
		
		for (CollisionPair pair : broadphaseCollisions) {
			Shape pA = pair.getA().getBody().getPolygon();
			Shape pB = pair.getB().getBody().getPolygon();
			if (pA.intersects(pB)) {
				collisions.add(getManifold(pair));
			}
		}
		
		return collisions;
	}
	
	private Manifold getManifold(CollisionPair pair) {

		LengthAndNormal info = getPenetration(pair);
		Manifold manifold = new Manifold(pair, info.distance, info.normal);
		return manifold;
	}
	
	private LengthAndNormal getPenetration(CollisionPair pair) {
		Shape a = pair.getA().getBody().getPolygon();
		Shape b = pair.getB().getBody().getPolygon();
		

		LengthAndNormal temp1 = getSupportVectorLength(a, b);
		LengthAndNormal temp2 = getSupportVectorLength(b, a);
		float supportLength1 = temp1.distance;
		float supportLength2 = temp2.distance;
		
		if (supportLength1 < supportLength2) {
			return temp1;
		} else {
			return temp2;
		}
	}
	
	private LengthAndNormal getSupportVectorLength(Shape a, Shape b) {
		
		double largestDistance = 0d;
		Vector2f normal = new Vector2f(0,0);
		
		for (int i = 0; i < a.getPointCount() - 1; i++) {
			Vector2f p1 = new Vector2f(a.getPoint(i));
			Vector2f p2 = new Vector2f(a.getPoint(i + 1));
			
			Line line = new Line(p1, p2);
			Vector2f normalCandidate = new Vector2f(line.getNormal(0)); // normal, for which we search the greatest distance in opp direction
			
			for (int j = 0; j < b.getPointCount(); j++) {
				Vector2f bPoint = new Vector2f(b.getPoint(i));
				
				// TODO: Check if Vector from line to point is in opp direction to the normal....
				if (true) {
					double candidate = pointToLineDistance(line.getStart(), line.getEnd(), bPoint);
					
					if (candidate > largestDistance) {
						largestDistance = candidate;
						normal = normalCandidate;
					}					
				}
			}
		}
		
		return new LengthAndNormal((float) largestDistance, normal);
		
	}
	
	private class LengthAndNormal{
		public final float distance;
		public final Vector2f normal;
		
		public LengthAndNormal(float distance, Vector2f normal) {
			this.distance = distance;
			this.normal = normal;
		}
	}
	
	List<HitResult> getHits(Shape a, Shape b) {
		List<HitResult> hits = new ArrayList<>();
		GeomUtil util = new GeomUtil();
		
		for (int i = 0; i < b.getPointCount() - 1; i++) {
			Vector2f p1 = new Vector2f(b.getPoint(i));
			Vector2f p2 = new Vector2f(b.getPoint(i + 1));
			
			Line line = new Line(p1, p2);
			Vector2f normal = new Vector2f(line.getNormal(0));
			HitResult hit = util.intersect(a, line);
			hits.add(hit);
		}
		
		return hits;
	}

	public void resolveCollisions(List<Manifold> collisions) {
		for (Manifold manifold : collisions) {
			resolveCollision(manifold);
		}
	}
	
	private void resolveCollision( Manifold manifold )
	{
		Entity A = manifold.getPair().getA();
		Entity B = manifold.getPair().getB();
		
	  // Calculate relative velocity
	  Vector2f rv = sub(B.getBody().getVelocity(), A.getBody().getVelocity());
	 
	  // Calculate relative velocity in terms of the normal direction
	  float velAlongNormal = dot( rv, manifold.getNormal() );
	 
	  // Do not resolve if velocities are separating
	  if(velAlongNormal > 0)
	    return;
	 
	  // Calculate restitution
	  float e = 1; // Math.min( A.restitution, B.restitution)
	 
	  // Calculate impulse scalar
	  float j = (velAlongNormal * -(1 + e));
	  j /= A.getBody().getMassData().getInverseMass() + B.getBody().getMassData().getInverseMass();
	 
	  // Apply impulse
	  Vector2f impulse = mul(manifold.getNormal(), j);
	  A.getBody().setVelocity(sub(A.getBody().getVelocity(), mul(impulse, A.getBody().getMassData().getInverseMass())));
	  B.getBody().setVelocity(add(B.getBody().getVelocity(), mul(impulse, B.getBody().getMassData().getInverseMass())));
	}
	
	public void render(Graphics g) throws SlickException {

		for (CollisionPair pair : broadphaseCollisions) {
			g.drawRect((int) pair.getA().getBody().getPolygon().getMinX(), (int) pair.getA().getBody().getPolygon().getMinY(), 100, 100);
		}
		for (Manifold m : collisions) {
			g.drawString("Penetration " + m.getPenetration(), (int) m.getPair().getA().getBody().getPolygon().getCenterX(), (int) m.getPair().getA().getBody().getPolygon().getCenterY());
		}
	}
}
