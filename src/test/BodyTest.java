package test;

import junit.framework.Assert;
import main.Body;

import org.junit.Test;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

public class BodyTest {

	
	@Test
	public void addVelocity() {
		Body body = new Body(new Rectangle(0, 0, 10, 10));
		Vector2f beginVelocity = body.getVelocity();
		
		body.addVelocity(new Vector2f(10, 10));
		
		Assert.assertEquals(new Vector2f(10, 10), body.getVelocity());
		
	}
}
