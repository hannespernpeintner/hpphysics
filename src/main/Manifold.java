package main;

import org.newdawn.slick.geom.Vector2f;

public class Manifold {
	private CollisionPair pair;
	private float penetration;
	private Vector2f normal;
	private Vector2f hitpoint;
	
	public Manifold(CollisionPair pair, float penetration, Vector2f normal) {
		this.pair = pair;
		this.penetration = penetration;
		this.normal = normal;
	}

	public CollisionPair getPair() {
		return pair;
	}

	public void setPair(CollisionPair pair) {
		this.pair = pair;
	}

	public float getPenetration() {
		return penetration;
	}

	public void setPenetration(float penetration) {
		this.penetration = penetration;
	}

	public Vector2f getNormal() {
		return normal;
	}

	public void setNormal(Vector2f normal) {
		this.normal = normal;
	}

//	public Vector2f getHitpoint() {
//		return hitpoint;
//	}
//
//	public void setHitpoint(Vector2f hitpoint) {
//		this.hitpoint = hitpoint;
//	}

}
