package main;

public class Material {
	private float density;
	private float restitution;
	
	public Material(float density, float restitution) {
		this.density = density;
		this.restitution = restitution;
	}
	
	public float getDensity() {
		return density;
	}
	public void setDensity(float density) {
		this.density = density;
	}
	public float getRestitution() {
		return restitution;
	}
	public void setRestitution(float restitution) {
		this.restitution = restitution;
	}
}
