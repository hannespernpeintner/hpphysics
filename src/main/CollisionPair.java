package main;

public class CollisionPair {
	
	private Entity a;
	private Entity b;
	
	public CollisionPair(Entity a, Entity b) {
		this.a = a;
		this.b = b;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CollisionPair) {
			CollisionPair partner = (CollisionPair) obj;
			if ((partner.getA().equals(a) && partner.getB().equals(b)) ||
				(partner.getA().equals(b) && partner.getB().equals(a))) {
				return true;
			}
		}
		return false;
	}

	public Entity getA() {
		return a;
	}

	public void setA(Entity a) {
		this.a = a;
	}

	public Entity getB() {
		return b;
	}

	public void setB(Entity b) {
		this.b = b;
	}

}
