package main;

import static math.Helper.mul;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

public class World {
	public static final float PPM = 100; // Pixel per meter world scale
	
	public final int fps = 100;
	public final float dt = 1.0f / fps;
	
	private Vector2f gravity = new Vector2f(0f, 9.81f);
	
	ArrayList<Entity> entities;
	Integrator integrator;
	CollisionDetector collisionDetector;
	
	public World() {
		entities = new ArrayList<Entity>();
		integrator = new Integrator();
		collisionDetector = new CollisionDetector();
	}
	
	public void update(float dt) {
		integrate(dt);
		handleCollisions(dt);
	}

	private void handleCollisions(float dt2) {
		collisionDetector.broadphaseCollisions = collisionDetector.getBroadPhaseCollisions(entities);
		collisionDetector.collisions = collisionDetector.getCollisions(collisionDetector.broadphaseCollisions);
		collisionDetector.resolveCollisions(collisionDetector.collisions);
	}

	private void integrate(float dt) {
		for (Entity entity: entities) {
			entity.getBody().setForce(mul(gravity, PPM));
			integrator.integrate(entity, dt);
		}
	}

	public void addEntity(Entity entity) {
		entities.add(entity);
	}

	public void render(Graphics g) throws SlickException {
		for (Entity entity : entities) {
			entity.render(g);
		}
		
		collisionDetector.render(g);
		
		g.drawString("Broadphases: " + collisionDetector.broadphaseCollisions.size(), 10, 40);
		g.drawString("Collisions: " + collisionDetector.collisions.size(), 10, 80);
	}

	public Vector2f getGravity() {
		return gravity;
	}

	public void setGravity(Vector2f gravity) {
		this.gravity = gravity;
	}

	public Integrator getIntegrator() {
		return integrator;
	}

	public void setIntegrator(Integrator integrator) {
		this.integrator = integrator;
	}

}
