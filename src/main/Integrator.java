package main;

import org.newdawn.slick.geom.Vector2f;

import static math.Helper.*;

public class Integrator {
	
	public Entity integrate(Entity entity, float dt) {
		
		Body body = entity.getBody();

		float im = body.getMassData().getInverseMass();
		Vector2f force = body.getForce();
		
		body.addVelocity(mul(mul(force, im), dt ));
		
		body.move(dt);
		return entity;
	}

}
