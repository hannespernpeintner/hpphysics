package test;

import junit.framework.Assert;
import math.Helper;

import org.junit.Test;
import org.newdawn.slick.geom.Vector2f;

public class MathTest {

	@Test
	public void VectorMul() {
		Vector2f a = new Vector2f(2, 2);
		Vector2f b = new Vector2f(2, 2);
		
		Vector2f result = Helper.mul(a, b);

		Assert.assertEquals(new Vector2f(4, 4), result);
		Assert.assertEquals(new Vector2f(2, 2), a);
		Assert.assertEquals(new Vector2f(2, 2), b);
	}
	@Test
	public void VectorScale() {
		Vector2f a = new Vector2f(2, 2);
		float b = 2.0f;
		
		Vector2f result = Helper.mul(a, b);

		Assert.assertEquals(new Vector2f(4, 4), result);
		Assert.assertEquals(new Vector2f(2, 2), a);
	}
	@Test
	public void VectorAdd() {
		Vector2f a = new Vector2f(2, 2);
		Vector2f b = new Vector2f(2, 2);

		Vector2f result = Helper.add(a, b);

		Assert.assertEquals(new Vector2f(4, 4), result);
		Assert.assertEquals(new Vector2f(2, 2), a);
	}
	

}
