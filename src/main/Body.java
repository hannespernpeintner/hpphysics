package main;

import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.geom.Vector2f;

import static math.Helper.*;

public class Body {
	private Shape polygon;
	private MassData massData;
	private Vector2f velocity;
	private Vector2f force;
	private Material material;
	private Circle boundingCircle;
	
	public Body(Rectangle rectangle) {
		this(rectangle, 1);
	}
	
	public Body(Rectangle rectangle, float mass) {
		polygon = rectangle;
		material = new Material(0.3f, 0.8f);
		massData = new MassData(mass, 1);
		setVelocity(new Vector2f(0, 0));
		force = new Vector2f(0,0);
		boundingCircle = new Circle(polygon.getCenterX(), polygon.getCenterY(), polygon.getBoundingCircleRadius());
	}

	public Circle getBoundingCircle() {
		return boundingCircle;
	}
	
	public Shape getPolygon() {
		return polygon;
	}
	public void setPolygon(Polygon polygon) {
		this.polygon = polygon;
	}
	public MassData getMassData() {
		return massData;
	}
	public void setMassData(MassData massData) {
		this.massData = massData;
	}
	public Vector2f getVelocity() {
		return velocity;
	}
	public void setVelocity(Vector2f velocity) {
		this.velocity = velocity;
	}
	public Vector2f getForce() {
		return force;
	}
	public void setForce(Vector2f force) {
		this.force = force;
	}
	public void addVelocity(Vector2f amount) {
		setVelocity(add(getVelocity(), amount));
	}
	public void move(Vector2f distance) {
		Transform t = Transform.createTranslateTransform(distance.x, distance.y);
		polygon = polygon.transform(t);
		boundingCircle.setCenterX(polygon.getCenterX());
		boundingCircle.setCenterY(polygon.getCenterY());
	}
	public void move(float dt) {
		move(mul(getVelocity(), dt));
	}
}
